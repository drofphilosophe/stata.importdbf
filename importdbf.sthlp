{smcl}
{* *! version 1.2.1  07mar2013}{...}
{findalias asfradohelp}{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "[R] help" "help help"}{...}
{viewerjumpto "Syntax" "examplehelpfile##syntax"}{...}
{viewerjumpto "Description" "examplehelpfile##description"}{...}
{viewerjumpto "Options" "examplehelpfile##options"}{...}
{viewerjumpto "Remarks" "examplehelpfile##remarks"}{...}
{viewerjumpto "Examples" "examplehelpfile##examples"}{...}
{title:Title}

{phang}
{bf:importdbf} {hline 2} Load data from files in dBase III and dBase III Plus format.


{marker syntax}{...}
{title:Syntax}

{p 8 17 2}
{cmdab:importdbf}
using {help filename}
{cmd:,} [{it:options}]

{synoptset 20 tabbed}{...}
{synopthdr}
{synoptline}
{syntab:Main}
{synopt:clear} Specifies that it is okay to replace the data in memory, even though the current data have not been saved to disk.{p_end}
{synopt:{opt deleted:obs(deletedaction)}} Specifies handling of observations tagged as deleted. 
	May be any of the following:{p_end}
{syntab:{it:deletedaction}}
	{synopt:{opt drop}} Ignores any observation with the deleted flag set to true. The default behavior.{p_end}
	{synopt:{opt keep}} Keeps all observations regardless of the deleted flag.{p_end}
	{synopt:{opt flag newvarname}} Keeps all observations and sets {it:newvarname} to 1 if the deleted flag
	is set and 0 otherwise.{p_end}




{marker description}{...}
{title:Description}

{pstd}
{cmd:importdbf} loads data from dBase III and dBase III Plus files into memory. 
It does not support (relatively) newer dBase IV or dBase V formats and will fail
when encountering unfamilar file formats. Numeric data types
are loaded as doubles, boolean data as bytes, dates are converted into {it:%td} format, and
memo fields are loaded as {it:strL}. Stata will {help compress} the dataset after loading.

{marker remarks}{...}
{title:Remarks}

{pstd}
This program is licensed under the Apache License, Version 2.0. 
See {browse "http://www.apache.org/licenses/LICENSE-2.0":www.apache.org/licenses/LICENSE-2.0}.
{p_end}

{pstd}
The dBase III Plus format is documented at 
{browse "http://ulisse.elettra.trieste.it/services/doc/dbase/DBFstruct.htm":ulisse.elettra.trieste.it/services/doc/dbase/DBFstruct.htm}.
{p_end}

{marker examples}{...}
{title:Examples}

{phang}{cmd:. clear}{p_end}
{phang}{cmd:. importdbf database.dbf}{p_end}

{phang}{cmd:. importdbf database.dbf, clear deletedobs(keep)}{p_end}

{phang}{cmd:. importdbf database.dbf, clear deletedobs(flag hasDeletedFlag)}{p_end}
