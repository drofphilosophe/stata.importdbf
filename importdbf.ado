/********************************************************
** importdbf
**

Copyright 2016 James Archsmith

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

**
** This program imports dBase database files into Stata datasets
** Every attempt to preserve the fidelity of the data in the original file is made
** Syntax of the command is similar to most Stata "import" commands
** The current version of the program only reads dBase III Plus files.
**
** REVISION HISTORY
** 20140730 - File Created
** 20140806 - Added code to read dBase III Plus with memo fields. 
**			  User-specified handling of the "deleted record" flag.
** 			  Other minor debugging.
********************************************************/
capture program drop importdbf

/* This is a wrapper program for the Mata code that actually imports the data. 
   It performs a few consistency checks and then calls the main mata function 
   to do all the heavy lifting. Mata expects no data in memory. 
   All other error checking should fall on Mata.
*/
program define importdbf, rclass
version 13.1
syntax using/, [clear DELETEDobs(string)]

	if c(changed) == 1 & "`clear'" != "clear" {
		display"{err}no; data in memory would be lost"
		exit 4
	}
	
	/***********************************************
	** Parse `deletedobs'
	************************************************
	** deletedobs tells the progam how to handled observations that are flagged as deleted
	**		deletedobs(drop) - drops deleted observations
	**		deletedobs(keep) - keeps all observations
	**		deletedobs(flag <varname>) - generates a 0/1 variable flagging deleted observations
	** default behavior is to drop deleted observations
	*/
	if "`deletedobs'" == "" | "`deletedobs'" == "drop" {
		local delete drop
	}
	else if "`deletedobs'" == "keep" {
		local delete keep
	}
	else if substr("`deletedobs'", 1,4) == "flag" {
		local delete flag
		local flagName : word 2 of `deletedobs'
		**check the varname is valid 
		if "`flagName'" != strtoname("`flagName'") {
			display "{res}`flagName' {err}is not a valid new Stata variable name"
			exit(198) /* Invalid varname */
		}
	}
	else {
		display "{err}Invalid specification for {it}deletedobs() {res}`deletedobs'"
		exit(198)
	}
	
	preserve	/* Preserve the data in case the mata function bails */
		clear
		mata:importDBF("`using'")
	restore, not /* The mata function worked, so we can ditch the last preserve */
	
	/****************************************************
	** Handle the observations flagged as deleted 
	*****************************************************/
	if "`delete'" == "drop" {
		quietly count if `deletedFlag' == 1
		if r(N) > 0 {
			display "{txt}File contains {res}" r(N) "{txt} observations flagged as deleted"
			display "{txt}These observations have been dropped"
			drop if `deletedFlag' == 1
			drop `deletedFlag'
		}
	}
	else if "`delete'" == "keep" {
		quietly count if `deletedFlag' == 1
		if r(N) > 0 {
			display "{txt}File contains {res}" r(N) "{txt} observations flagged as deleted"
			display "{txt}Keeping deleted observatons"
			drop `deletedFlag'
		}
		else {
			display "{txt}Warning: File contains no observations flagged as deleted."
		}
	}
	else if "`delete'" == "flag" {
		quietly count if `deletedFlag' == 1
		display "{txt}File contains {res}" r(N) "{txt} observations flagged as deleted"
		display "{txt}Saving deleted status in {res}`flagName'"
		rename `deletedFlag' `flagName'
	}
	else {
		/* This branch of code should never be executed, it's just here to catch any programming errors */
		display "{err}Unknown delete flag action {res}`delete'"
		display "{err}This should never happen"
		exit(9999)
	}
	
	/* Compress the dataset.  In general the Mata code should be very good about allocating space 
	   for variables and the compress comand should, hopefully, do nothing */
	quietly compress

end

mata 

	/* This function coverts a binary string (perhaps read from a file) into the corrisponding
	   integer in little endian notation. */
	real scalar function LEtoInt(string d) {
		real scalar i
		real scalar result
		real scalar temp
		real scalar len
		
		len = strlen(d)
		result = 0
		
		for(i=1;i<=len;i++) {
			result = result + ascii(substr(d, i, 1))*256^(i-1)
		}
		return(result)
	}
	
	/* Main program. This function determines the appropriate DBF format of the file and then calls
	   The correct import function */
	void function importDBF(string scalar filename) {
		real scalar fh	/* file handle */
		real scalar fileVersion /* dBase file version info */
		string memo_filename /* filename of the memo file, if needed */
		real scalar memo_fh	/* file handle of the memo file, if needed */
		
		printf("{txt}\ndBase (DBF) file coverter\n")
		
		/****************************************
		** Open the specified file
		*****************************************/
		fh = _fopen(filename, "r")
		/* Test that the file was actually opend for reading */
		if(fh < 0) {
			printf("{err}Unable to open file {res}%s\n", filename)
			printf("{err}Error: %s\n", ferrortext(fh))
			exit(603)	/* Stata code for file could not be opened */
		}
		else {
			/* If the file handle is positive, we can read the file. Tell the user. */
			printf("{txt}Reading file {res}%s\n", filename)
		}
		
		/*********************************
		** Determine the dBase file version and call the appropriate import function
		**********************************/
		
		/* Read the first byte of the file. It contains the version of the dBase file. */
		fileVersion = LEtoInt(fread(fh,1))
		
		if(fileVersion == 3) importDBFv3Plus(fh,0)	
		else if(fileVersion == 131) {
			/* fileVersion = 131 (0x83) is dBase III Plus with memo. */
			/* check that the memo file exists */
			memo_filename = substr(filename, 1, strlen(filename) - 3) + "dbt"
			if(!fileexists(memo_filename)) {
				printf("{err}dBase Memo file {res}%s {err}not found.\n", memo_filename)
				exit(603)
			}
			memo_fh = fopen(memo_filename, "r")
			if(memo_fh == 0) {
				printf("{err}Memo file {res}%s {err}could not be opened.\n", memo_filename)
				exit(603)
			}
			
			importDBFv3Plus(fh, memo_fh)
			fclose(memo_fh)
		}
		else {
			printf("{err}File version %f is not supported at this time\n", fileVersion)
			exit(609)	/* File in unsupported format */
		}
		
		fclose(fh)
	}
	
	/* The following code imports dBase III Plus files. The specification is published at:
	   http://ulisse.elettra.trieste.it/services/doc/dbase/DBFstruct.htm
	*/
	void function importDBFv3Plus(real scalar fh, real scalar memo_fh) {
		real scalar lastUpdateDate /* dbase file last update date */
		real scalar numRecords /* number of records (observations) in data file */
		real scalar headerSize /* dBase file header size in bytes */
		real scalar fieldCount /* number of dBase fields (variables) */
		real scalar recordSize /* length of each record in bytes */
		string colvector fieldNames /* colVec of dBase field names */
		string colvector fieldTypes /* colVec of Stata variable types */
		real colvector fieldLength /* colVec of dbase field lengths */
		real colvector fieldDecimal /* colvec of dBase decimal positions */
		string ft /* temporary storage for Stata field type */
		real scalar varNum /* temporary storage for Stata variable number */
		string deletedFlagName /* holds stata varname for the deleted flag */
		real scalar deletedFlagNum /* hold stata variable number for deleted flag */
		real scalar term /* Storage for record terminators */
		string fieldData /* Temporary storage for data elements read from the dBase file */
		real scalar year
		real scalar month
		real scalar day /* Temporary storage for date field data */
		
		printf("{txt}Importing dBase III Plus file\n")


		/******************************************************************
		** PARSE FILE HEADER
		*******************************************************************/
		lastUpdateDate = LEtoInt(fread(fh,3))
		numRecords = LEtoInt(fread(fh,4))
		printf("{txt}Reading {res}%f {txt}records\n", numRecords)
		
		headerSize = LEtoInt(fread(fh,2))
		fieldCount = (headerSize - 33) / 32	/*Determine the number of fields. Each field header is 32 bytes */
		printf("{txt}Parsing {res}%f {txt}variables\n", fieldCount)
		
		recordSize = LEtoInt(fread(fh,2))
		
		fseek(fh,32,-1)	/* Skip past reserved fields, they end at byte 32 (measured from the begining of the file) */
		
		/*****************************************************************
		** PARSE FIELD DESCRIPTIORS
		******************************************************************/
		/*Read each field header into a matrix*/
		fieldNames = J(fieldCount,1,"")
		fieldTypes = J(fieldCount,1,"")
		fieldLength = J(fieldCount,1,0)
		fieldDecimal = J(fieldCount,1,0)
		
		/* Make the dataset long enough for all the observations */
		st_addobs( numRecords - st_nobs())
		
		/* Parse all of the field descriptor records and create variables in the dataset */
		for(i=1;i<=fieldCount;i++) {
			fieldNames[i] = fread(fh, 11)
			fieldTypes[i] = fread(fh, 1)
			fseek(fh,4,0) /* Skip data addreess */
			fieldLength[i] = LEtoInt(fread(fh,1))
			fieldDecimal[i] = LEtoInt(fread(fh,1))
			fseek(fh,14,0) /* Skip other irrelevant entries in the variable table */
			
			/* Now create a Stata variable for this field */
			if(fieldTypes[i] == "C") {
				/* create a stata string that is the appropriate length for the data in the file */
				ft = "str" + strofreal(fieldLength[i])
				varNum = st_addvar(ft, strtoname(fieldNames[i]))
			}
			else if(fieldTypes[i] == "M") {
				if(memo_fh <= 0) {
					printf("{err}Memo field {res}%s {err}specified in non-memo formatted file.\n", strtoname(fieldNames[i]))
					printf("{err}The file is corrupt or not a dBase III Plus file.\n")
					exit(608)
				}
				varNum = st_addvar("strL", strtoname(fieldNames[i]))
			}
			else if(fieldTypes[i] == "D") {
				varNum = st_addvar("int", strtoname(fieldNames[i]))
				/* Dates will be formatted %td. Take care of that now */
				st_varformat(strtoname(fieldNames[fCount], "%td"))
			}
			else if(fieldTypes[i] == "N") {
				varNum = st_addvar("double", strtoname(fieldNames[i]))
			}
			else if(fieldTypes[i] == "L") {
				varNum = st_data(., st_addvar("byte", strtoname(fieldNames[i])))
			}
			else {
				/* Throw an error becuase the field type is unknown */
				printf("{err}The header specifies an unknown field type of {res}%s {err} for variable {res}%s\n", fieldTypes[i], strtoname(fieldNames[i]))
				printf("{err}This means the data file header is corrupt or this is not a valid dBase III Plus file.\n")
				exit(688)
			}
		}
		
		/* Create a temporary variable to house the "deleted" flag.
		   Then return it in the local deletedFlag */
		deletedFlagName = st_tempname()
		st_local("deletedFlag", deletedFlagName)
		deletedFlagNum = st_addvar("byte", deletedFlagName)
		
		/* Read out the header terminator and assert it is 0x0D. This means we read to the end of the header */
		term = ascii(fread(fh,1))
		if( term != 13 ) {
			printf("{err}The data file header is corrupt or this is not a valid dBase III Plus file.\n")
			exit(688)	/* File is corrupt error code */
		}
		
		/*************************************************************************
		** READ DATA RECORDS
		**************************************************************************/
		
		/* Now let's loop through every record and read out the data into some matrixes */
		for(recCount=1;recCount<=numRecords;recCount++) {
			/* read the record delimiter. It should be 0x20 (saved) or 0x2A (deleted) */
			term = ascii(fread(fh,1))
			if(term == 32) st_store(recCount, deletedFlagNum, 0)
			else if(term == 42) st_store(recCount, deletedFlagNum, 1)
			else {
				printf("{err}Unknown record delimiter {res}%f\n", term)
				printf("{err}Either the file is corrupt or this is not a valid dBase III Plus file.\n")
				exit(688) /* File is corrupt error code */
			}
			/* For each record, read all of the field data */
			for(fCount=1;fCount<=fieldCount;fCount++) {
				fieldData = fread(fh, fieldLength[fCount])
				if(fieldTypes[fCount] == "C") {
					st_sstore(recCount,fCount, fieldData)
				}
				else if(fieldTypes[fCount] == "M") {
					/* determine the starting point of the memo data in the file */
					fseek(memo_fh, 512*strtoreal(fieldData),-1)
					
					/* read 512-byte blocks until we read a field terminator (char(26) + char(26)) */
					loopFlag = 1
					fieldData = ""
					while(loopFlag == 1) {
						blockData = fread(memo_fh, 512)
						if(strpos(blockData, char(26) + char(26)) > 0 ) {
							fieldData = fieldData + substr(blockData, 1, strpos(blockData, char(26) + char(26))-1)
							loopFlag = 0
						}
						else {
							fieldData = fieldData + blockData
						}
					}
					st_sstore(recCount,fCount, fieldData)
				}
				else if(fieldTypes[fCount] == "D") {
					year = strtoreal(substr(fieldData,1,4))
					month = strtoreal(substr(fieldData,5,2))
					day = strtoreal(substr(fieldData,7,2))
					st_store(recCount,fCount,mdy(year, month, day))
				}
				else if(fieldTypes[fCount] == "L") {
					if(strupper(fieldData) == "Y" | strupper(fieldData) == "T") st_store(recCount,fCount,1) 
					else if(strupper(fieldData) == "N" | strupper(fieldData) == "F") st_store(recCount,fCount,0)
				}
				else if(fieldTypes[fCount] == "N") {
					st_store(recCount,fCount,strtoreal(fieldData) ) 
				}
			}
		}
	}
	
end




